// hello
package main

import (
	"fmt"
	_ "time"
)

func main() {
	fmt.Println("Hello World!")

	var i int
	var s string
	var age int = 10
	var name string = "Maria"

	// var i1, i2 int
	// var s1, s2 string
	// var age1, age2 int = 10, 20
	// var name1, name2 string = "Maria1", "Maria2"
	// var age3, age4 = 30, 40
	// var name3, name4 = "Maria3", "Maria4"
	// age5, age6 := 50, 60
	// name5, name6 := "Maria5", "Maria6"

	// var (
	// 	x, y        int = 30, 50
	// 	age7, name7     = 10, "tsusai"
	// )

	_ = i
	_ = s
	_ = age
	_ = name

	var kk int = 9223372030000000000
	for i := 0; i < 9000000000000000000; i++ {
		if kk > kk+1 {
			print(kk)
			print("\n")
			print(kk + 1)
			print("\n")
		}
		kk = kk + 1
	}
}
